#!/bin/bash
echo
echo

aVar=$(date)
echo $aVar

echo
echo

if [ ! -d "./output" ]; then
	mkdir ./output
fi

touch ./output/"$aVar"
ls -al ./output

echo
echo
echo "Building the cpp binary!"
g++ helloworld.cpp -o helloworld.exe
echo
echo
echo "Did the build work?"
echo
echo

pwd
ls -al


